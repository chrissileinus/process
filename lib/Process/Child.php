<?php
/* Process/Child.php - Class for running child processes
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

/* Parts:
 *   class Child      Prepare a child of a parent
 */

namespace	Process;

class	Child {
	protected	$connection	=	null;
	protected	$name				=	false;
	protected	$lastTick		=	0;

	//	__construct	START	-------------------------------------------------------
	final	public		function	__construct() {
		$this->name		=	null ==! getenv('client')?
										getenv('client'):
										$this->name;

		if(!$this->name) {
			throw	new	Warning("This process is not running as a child of aParent!?");
		}

		$this->connection	=	stream_socket_client("unix:///tmp/child.{$this->name}.sock", $errorNo, $errorStr, 1);
		if(!\is_resource($this->connection)) {
			throw	new	Exception("Could not open Socket. ({$errorNo}) {$errorStr}");
		}
		stream_socket_sendto($this->connection, posix_getpid().Config::EOT);
		stream_set_blocking($this->connection, false);
	}
	//	__construct	END		-------------------------------------------------------

	//	__destruct	START	-------------------------------------------------------
	final	public		function	__destruct() {
		$this->close();
	}
	//	__destruct	END		-------------------------------------------------------

	/**	close		START	-------------------------------------------------------
	* Tell the child to close.
	*/
	final	public		function	close() {
	}
	//	close		END		-------------------------------------------------------

	/*	on*			START	-------------------------------------------------------
	 *	This a placeholder an get called on different situations
	 *
	.*	Example:
	.*	while(WebSocket\Server->run(function($client) {
	 *		$client->receive()
	.*	})) {}
	*/
	public					function	onReceive($data) {}

	public					function	onTick() {}
	//	on*			END		-------------------------------------------------------

	/*	run			START	-------------------------------------------------------
	 *	This method is to acct as a expression in while.
	 *	The callback runs for all connections once and get the Connection Object to work with.
	 *
	.*	Example:
	.*	while(WebSocket\Server->run(function($client) {
	 *		$client->receive()
	.*	})) {}
	*/
	final	public		function	run() {
		if(!$this->name)	return	true;

		if($data = $this->receive()) {
			$this->onReceive($data);
		}

		$this->onTick();

		return	true;
	}
	//	run			END		-------------------------------------------------------

	/**	send		START	-------------------------------------------------------
	* Send Child some data
	*/
	final	public		function	send($data) {
		if(!$this->name)	return	0;

		if(is_array($data)) {
			$data	=	json_encode($data);
		}

		if(!@stream_get_meta_data($this->connection)) {
			throw	new	Exception("Lost connection!");
		}

		$sleep	=	0.05;
		$read		=	null;
		$write	=	[$this->connection];
		$except	=	null;
		if(@\stream_select($read, $write, $except, 0, $sleep * 1000000) > 0) {
			return	stream_socket_sendto($this->connection, $data.Config::EOT);
		}
		return	0;
	}
	//	write		END		-------------------------------------------------------

	/**	receive		START	-------------------------------------------------------
	* Receive data from Child
	*/
	final	public		function	receive() {
		if(!$this->name)	return	'';

		if(!@stream_get_meta_data($this->connection)) {
			throw	new	Exception("Lost connection!");
		}

		$data	=	stream_get_line($this->connection, Config::LENGTH, Config::EOT);

		$array	=	json_decode($data, true);
		if(json_last_error() === JSON_ERROR_NONE)	$data	=	$array;
		elseif(strlen($data)	>	50)				var_dump(json_last_error_msg());

		return	$data;
	}
	//	receive		END		-------------------------------------------------------


	/**	socket		START	-------------------------------------------------------
	*/
	final	public		function	socket() {
		if(!$this->name)	return	null;

		return	$this->connection;
	}
	//	receive		END		-------------------------------------------------------
}
?>