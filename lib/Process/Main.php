<?php
/* Process/Main.php - Class for running child processes
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

/* Parts:
 *   class Main      Prepare a parent for children
 */

namespace	Process;

class	Main {
	protected	$children	=	[];
	protected	$lastTick	=	0;

	//	__construct	START	-------------------------------------------------------
	final	public		function	__construct() {
	}
	//	__construct	END		-------------------------------------------------------

	//	__destruct	START	-------------------------------------------------------
	final	public		function	__destruct() {
		$this->close();
	}
	//	__destruct	END		-------------------------------------------------------

	/**	close		START	-------------------------------------------------------
	* Tell the children to close.
	*/
	final	public		function	close($name = null) {
		foreach($this->children as $key => $child){
			unset($this->children[$key]);
		}
	}
	//	close		END		-------------------------------------------------------

	/**	genChild	START	-------------------------------------------------------
	* generate a child process.
	*/
	final	public		function	genChild(string $command, string $name) {
		if(@is_object($this->children[$name])) {
			throw	new	Exception("{$name} is already a name of a child!");
		}

		$this->children[$name]	=	new	Main\Child($command, $name);
	}
	//	genChild	END		-------------------------------------------------------

	/**	startChild	START	-------------------------------------------------------
	* Stop a child process.
	*/
	final	public		function	startChild(string $name) {
		if(@is_object($this->children[$name])) {
			$this->children[$name]->start();
			return	true;
		}
	}
	//	startChild	END		-------------------------------------------------------

	/**	stopChild	START	-------------------------------------------------------
	* Stop a child process.
	*/
	final	public		function	stopChild(string $name) {
		if(@is_object($this->children[$name])) {
			$this->children[$name]->stop();
			return	true;
		}
	}
	//	stopChild	END		-------------------------------------------------------

	/*	on*			START	-------------------------------------------------------
	 *	This a placeholder an get called on different situations
	 *
	.*	Example:
	.*	while(WebSocket\Server->run(function($client) {
	 *		$client->receive()
	.*	})) {}
	*/
	public					function	onReceive($child, $data) {}

	public					function	onTick() {}
	//	on*			END		-------------------------------------------------------

	/*	run			START	-------------------------------------------------------
	 *	This method is to acct as a expression in while.
	 *	The callback runs for all connections once and get the Connection Object to work with.
	 *
	.*	Example:
	.*	while(WebSocket\Server->run(function($client) {
	 *		$client->receive()
	.*	})) {}
	*/
	final	public		function	run(float $timeout = 0) {
		$read		=	$this->getAllReadSockets();
		$write	=	null;
		$except	=	null;
		if(@\stream_select($read, $write, $except, 0, $timeout * 1000000) > 0) {
			foreach($this->children as $key => $child){
				try {
					if($data = $child->receive()) {
						$this->onReceive($child, $data);
					}
				}
				catch(Exception $e) {
					$child->stop();
				}
			}
		}

		$this->onTick();

		return	true;
	}
	//	run			END		-------------------------------------------------------

	/**	send		START	-------------------------------------------------------
	* Send Child some data
	*/
	final	public		function	send($childName, $data) {
		if(!is_object($this->children[$childName])) {
			throw	new	Exception("{$childName} is not my child!");
		}

		try {
			$this->children[$childName]->send($data);
		}
		catch(Exception $e) {
			$this->children[$childName]->stop();
		}
	}
	//	write		END		-------------------------------------------------------

	/*	sendBroadcast	START	-------------------------------------------------------
	 *	Send all Children some data
	*/
	final	public		function	sendBroadcast($data) {
		foreach($this->children as $child) {
			try {
				$child->send($data);
			}
			catch(Exception $e) {
				$child->stop();
			}
		}
	}
	//	sendAll		END		-------------------------------------------------------

	final	public		function	getAllReadSockets() {
		$sockets	=	[];
		foreach($this->children as $child) {
			$sockets[]	=	$child->getReadSockets();
		}
		return	$sockets;
	}

	final	public		function	getAllChildrenNames() {
		return	array_keys($this->children);
	}

}
?>