<?php
/* Process/Main/Child.php - Class for running child processes
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

/* Parts:
 *   class Child      Control a child in a parent process
 */

namespace	Process\Main;

use			\Process\Config;

class	Child {
	protected	$process		=	null;
	protected	$socket			=	null;
	protected	$command		=	'';
	protected	$name				=	'';
	protected	$connection	=	null;
	protected	$pid				=	null;
	protected	$pipes			=	[];

	private		$readable		=	['name', 'pid'];

	//	__construct	START	-------------------------------------------------------
	final	public		function	__construct(string $command, string $name) {
		$this->command	=	$command;
		$this->name			=	$name;

		$this->start();
	}
	//	__construct	END		-------------------------------------------------------

	//	__destruct	START	-------------------------------------------------------
	final	public		function	__destruct() {
		$this->stop();
	}
	//	__destruct	END		-------------------------------------------------------

	/**	start		START	-------------------------------------------------------
	* Start the child.
	*/
	final	public		function	start() {
		if(is_resource($this->process)) {
			throw	new	Exception("Process already running!");
		}

		$this->process		=	proc_open($this->command, [1 => STDOUT, 2 => STDERR], $this->pipes, NULL, ['client' => $this->name]);
		if(!\is_resource($this->process)) {
			throw	new	Exception("Could not open Process.");
		}

		$this->socket			=	stream_socket_server("unix:///tmp/child.{$this->name}.sock", $errorNo, $errorStr);
		if(!\is_resource($this->socket)) {
			throw	new	Exception("Could not open Socket. ({$errorNo}) {$errorStr}");
		}

		$this->connection	=	stream_socket_accept($this->socket);
		$this->pid				=	stream_get_line($this->connection, Config::LENGTH, Config::EOT);
		stream_set_blocking($this->connection, false);
	}
	//	start		END		-------------------------------------------------------

	/**	stop		START	-------------------------------------------------------
	* Tell the child to stop.
	*/
	final	public		function	stop() {
		if(is_resource($this->process)) {
			posix_kill($this->pid, SIGTERM);
			do {
				$status	=	\proc_get_status($this->process);
				// var_dump($status);
			}
			while($status['running']);
			\proc_terminate($this->process);
		}


		if(is_resource($this->connection))							\fclose($this->connection);
		if(is_resource($this->socket))									\fclose($this->socket);
		if(file_exists("/tmp/child.{$this->name}.sock"))\unlink("/tmp/child.{$this->name}.sock");

		$this->process		=	null;
		$this->connection	=	null;
		$this->socket			=	null;
	}
	//	stop		END		-------------------------------------------------------

	/**	Status		START	-------------------------------------------------------
	* Tell the child to stop.
	*/
	final	public		function	state() {
		if(!is_resource($this->process))	return	false;
		return	\proc_get_status($this->process)['running'];
	}
	//	stop		END		-------------------------------------------------------

	/**	send		START	-------------------------------------------------------
	* Send Child some data
	*/
	final	public		function	send($data) {
		if(is_array($data)) {
			$data	=	json_encode($data);
		}

		if(!@stream_get_meta_data($this->connection)) {
			throw	new	Exception("Lost connection!");
		}

		return	stream_socket_sendto($this->connection, $data.Config::EOT);
	}
	//	write		END		-------------------------------------------------------

	/**	receive		START	-------------------------------------------------------
	* Receive data from Child
	*/
	final	public		function	receive() {
		if(!@stream_get_meta_data($this->connection)) {
			throw	new	Exception("Lost connection!");
		}

		$data		=	stream_get_line($this->connection, Config::LENGTH, Config::EOT);

		$array	=	json_decode($data, true);
		if(json_last_error() === JSON_ERROR_NONE)	$data	=	$array;

		return	$data;
	}
	//	receive		END		-------------------------------------------------------

	final	public		function	__get($key) {
		if(in_array($key, $this->readable))
			return	$this->$key;
		else
			return	null;
	}

	final	public		function	getReadSockets() {
		return	$this->connection;
	}
}

?>