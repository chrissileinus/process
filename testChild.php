<?php
require __DIR__ . '/vendor/autoload.php';

$child	=	new class extends \Process\Child {
	public	function	onReceive($data) {

		$data	=	json_encode($data);

		echo str_pad("parent", 20, " ", STR_PAD_LEFT) . " {$data} " . PHP_EOL;
	}

	public	function	onTick() {
		$this->send("Hallo Eltern {$this->name}");
	}
};


$nextRun	=	microtime(true) + 0.1;

while($child->run()) {
	pcntl_signal_dispatch();
	$nextRun	=	$nextRun + 0.1;




	time_sleep_until($nextRun);
}
?>