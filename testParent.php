<?php
require __DIR__ . '/vendor/autoload.php';

$signalHandler	=	function($signalNo) {
	echo	"Signal handler START Parent".PHP_EOL;

	global	$parent;
	unset($parent);

	echo	"Signal handler END Parent".PHP_EOL;
	exit;
};
pcntl_signal(SIGINT, $signalHandler);
pcntl_signal(SIGTERM, $signalHandler);


$parent	=	new	class() extends \Process\Main {
	public	function	onReceive($client, $data) {

		$data	=	json_encode($data);

		echo str_pad($client->name, 20, " ", STR_PAD_LEFT) . " {$data} " . PHP_EOL;
	}

	public	function	onTick() {
		$this->sendBroadcast("Hallo Kinder");
	}
};
$parent->genChild('php testChild.php', 'One');
$parent->genChild('php testChild.php', 'Two');
$parent->genChild('php testChild.php', 'Three');


$nextRun	=	microtime(true) + 0.1;

while($parent->run()) {
	pcntl_signal_dispatch();
	$nextRun	=	$nextRun + 0.1;




	time_sleep_until($nextRun);
}

?>